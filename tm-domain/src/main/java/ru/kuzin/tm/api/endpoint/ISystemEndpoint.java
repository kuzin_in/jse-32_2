package ru.kuzin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.kuzin.tm.dto.request.ApplicationAboutRequest;
import ru.kuzin.tm.dto.request.ApplicationVersionRequest;
import ru.kuzin.tm.dto.response.ApplicationAboutResponse;
import ru.kuzin.tm.dto.response.ApplicationVersionResponse;

public interface ISystemEndpoint {

    @NotNull
    ApplicationAboutResponse getAbout(@NotNull ApplicationAboutRequest request);

    @NotNull
    ApplicationVersionResponse getVersion(@NotNull ApplicationVersionRequest request);

}