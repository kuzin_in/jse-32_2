package ru.kuzin.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.kuzin.tm.api.client.*;
import ru.kuzin.tm.api.repository.ICommandRepository;
import ru.kuzin.tm.api.service.ICommandService;
import ru.kuzin.tm.api.service.ILoggerService;
import ru.kuzin.tm.api.service.IPropertyService;
import ru.kuzin.tm.api.service.IServiceLocator;
import ru.kuzin.tm.client.*;
import ru.kuzin.tm.command.AbstractCommand;
import ru.kuzin.tm.command.server.ConnectCommand;
import ru.kuzin.tm.command.server.DisconnectCommand;
import ru.kuzin.tm.exception.system.ArgumentNotSupportedException;
import ru.kuzin.tm.exception.system.CommandNotSupportedException;
import ru.kuzin.tm.repository.CommandRepository;
import ru.kuzin.tm.service.CommandService;
import ru.kuzin.tm.service.LoggerService;
import ru.kuzin.tm.service.PropertyService;
import ru.kuzin.tm.util.SystemUtil;
import ru.kuzin.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.kuzin.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService(propertyService);

    @Getter
    @NotNull
    private final ISystemEndpointClient systemEndpointClient = new SystemEndpointClient();

    @Getter
    @NotNull
    private final IDomainEndpointClient domainEndpointClient = new DomainEndpointClient();

    @Getter
    @NotNull
    private final IUserEndpointClient userEndpointClient = new UserEndpointClient();

    @Getter
    @NotNull
    private final IProjectEndpointClient projectEndpointClient = new ProjectEndpointClient();

    @Getter
    @NotNull
    private final ITaskEndpointClient taskEndpointClient = new TaskEndpointClient();

    @Getter
    @NotNull
    private final FileScanner fileScanner = new FileScanner(this);

    @Getter
    @NotNull
    private final IEndpointClient connectionEndpointClient = new ConnectionEndpointClient();

    @Getter
    @NotNull
    private final IAuthEndpointClient authEndpointClient = new AuthEndpointClient();

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes = reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) registry(clazz);
    }

    private void connect() {
        processCommand(ConnectCommand.NAME);
    }

    private void disconnect() {
        processCommand(DisconnectCommand.NAME);
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        final AbstractCommand command = clazz.newInstance();
        registry(command);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void prepareStartup() {
        initPID();
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        fileScanner.start();
        connect();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
        fileScanner.stop();
        disconnect();
    }

    private void processArguments(@Nullable final String argument) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    private boolean processArguments(@Nullable final String[] arguments) {
        if (arguments == null || arguments.length < 1) return false;
        @Nullable final String arg = arguments[0];
        processArguments(arg);
        return true;
    }

    public void processCommand(@Nullable final String command) {
        processCommand(command, true);
    }

    public void processCommand(@Nullable final String command, boolean checkRoles) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void processCommands() {
        try {
            System.out.println("ENTER COMMAND: ");
            @NotNull final String command = TerminalUtil.nextLine();
            processCommand(command);
            System.out.println("[OK]");
            loggerService.command(command);
        } catch (@NotNull final Exception e) {
            loggerService.error(e);
            System.out.println("[FAIL]");
        }
    }

    public void run(@Nullable final String[] args) {
        if (processArguments(args)) System.exit(0);
        prepareStartup();
        while (!Thread.currentThread().isInterrupted()) processCommands();
    }

}