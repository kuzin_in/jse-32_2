package ru.kuzin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.kuzin.tm.api.client.IAuthEndpointClient;
import ru.kuzin.tm.api.client.IUserEndpointClient;
import ru.kuzin.tm.command.AbstractCommand;
import ru.kuzin.tm.exception.entity.UserNotFoundException;
import ru.kuzin.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    public IUserEndpointClient getUserEndpoint() {
        return serviceLocator.getUserEndpointClient();
    }

    @NotNull
    public IAuthEndpointClient getAuthEndpoint() {
        return serviceLocator.getAuthEndpointClient();
    }

    protected void showUser(final User user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
    }

    @Override
    public String getArgument() {
        return null;
    }

}