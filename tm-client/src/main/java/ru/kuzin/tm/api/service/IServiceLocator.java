package ru.kuzin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.kuzin.tm.api.client.*;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IEndpointClient getConnectionEndpointClient();

    @NotNull
    IAuthEndpointClient getAuthEndpointClient();

    @NotNull
    IDomainEndpointClient getDomainEndpointClient();

    @NotNull
    IProjectEndpointClient getProjectEndpointClient();

    @NotNull
    ITaskEndpointClient getTaskEndpointClient();

    @NotNull
    ISystemEndpointClient getSystemEndpointClient();

    @NotNull
    IUserEndpointClient getUserEndpointClient();

}