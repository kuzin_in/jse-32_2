package ru.kuzin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.kuzin.tm.dto.request.UserLoginRequest;
import ru.kuzin.tm.dto.request.UserLogoutRequest;
import ru.kuzin.tm.dto.request.UserViewProfileRequest;
import ru.kuzin.tm.dto.response.UserLoginResponse;
import ru.kuzin.tm.dto.response.UserLogoutResponse;
import ru.kuzin.tm.dto.response.UserViewProfileResponse;

public interface IAuthEndpoint {

    @NotNull
    UserLoginResponse login(@NotNull UserLoginRequest request);

    @NotNull
    UserLogoutResponse logout(@NotNull UserLogoutRequest request);

    @NotNull
    UserViewProfileResponse profile(@NotNull UserViewProfileRequest request);

}