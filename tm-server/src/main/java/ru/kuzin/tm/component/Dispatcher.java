package ru.kuzin.tm.component;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kuzin.tm.api.endpoint.Operation;
import ru.kuzin.tm.dto.request.AbstractRequest;
import ru.kuzin.tm.dto.response.AbstractResponse;
import ru.kuzin.tm.exception.system.CommandNotSupportedException;

import java.util.LinkedHashMap;
import java.util.Map;

public class Dispatcher {

    @NotNull
    private final Map<Class<? extends AbstractRequest>, Operation<?, ?>> map = new LinkedHashMap<>();

    public <RQ extends AbstractRequest, RS extends AbstractResponse> void registry(
            @NotNull final Class<RQ> reqClass, @NotNull final Operation<RQ, RS> operation
    ) {
        map.put(reqClass, operation);
    }

    @NotNull
    @SuppressWarnings({"unchecked", "rawtypes"})
    public Object call(@NotNull final AbstractRequest request) {
        @Nullable final Operation operation = map.get(request.getClass());
        if (operation == null) throw new CommandNotSupportedException();
        return operation.execute(request);
    }

}